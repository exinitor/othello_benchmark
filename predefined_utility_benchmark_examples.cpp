// gtest
#include <benchmark/benchmark.h>   // googletest header file

// cc library
#include <engine.h>

namespace detail
{

  // Declarations
  auto generateStartPositionBitBoard();

  // Definitions
  auto generateStartPositionBitBoard()
  {

    const othello::BitPieces white_starter_pieces{"00000000"
                                                  "00000000"
                                                  "00000000"
                                                  "00010000"
                                                  "00001000"
                                                  "00000000"
                                                  "00000000"
                                                  "00000000"};
    const othello::BitPieces black_starter_pieces{"00000000"
                                                  "00000000"
                                                  "00000000"
                                                  "00001000"
                                                  "00010000"
                                                  "00000000"
                                                  "00000000"
                                                  "00000000"};

    return othello::BitBoard{white_starter_pieces, black_starter_pieces};
  }
}   // namespace detail

static void leagalMovesFromInitBoard(benchmark::State& state)
{
  const auto bitboard = detail::generateStartPositionBitBoard();

//  for (auto _ : state) {
  while( state.KeepRunning()) {
    auto legal_moves
      = othello::utility::legalMoves(bitboard, othello::PlayerId::One);
    benchmark::DoNotOptimize(legal_moves);
  }
}

BENCHMARK(leagalMovesFromInitBoard);

BENCHMARK_MAIN()
